import 'package:http/http.dart';


class CreditCardModel{
  final String _cardNo,_expiryDate,_cvv,_logo;
  CreditCardModel(this._cardNo,
      this._logo,
      this._expiryDate,
      this._cvv):
      assert(_cardNo.length==9);


  String get cardNo
  {
    String fakeCardNo = "3223";
    return fakeCardNo;
  }


  String get logo => _logo;

  String get cvv => _cvv;

  String get expiryDate => _expiryDate;

}